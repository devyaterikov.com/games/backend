const express = require('express');
const path = require('path');
const createError = require('http-errors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const debug = require('debug')('app:debug');
const morgan = require('morgan');

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use('/static', express.static(path.join(__dirname, 'static')));

require('./routes/index')(app);

app.use((req, res) => {
    throw createError(404, 'Not Found');
});

app.use((err, req, res, next) => {
    res.status(err.status).send(`Status: ${err.status}<br>Message: ${err.message}`);
});

app.listen(3000);
