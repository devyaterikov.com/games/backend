const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).send('This is tic-tac-toe main page');
});

module.exports = router;