const TicTacToe = require('./tic-tac-toe');

module.exports = function(app) {
    app.get('/', (req, res, next) => {
        res.status(200).send('Hello, world!');
    });
    app.use('/tic-tac-toe', TicTacToe);
};